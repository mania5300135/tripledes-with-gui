import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {
        createGUI();
    }


    private static void createGUI () {
        JFrame frame = new JFrame();
        frame.setTitle("Основное окно");
        TextScreen ts = new TextScreen();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Font font = new Font("Verdana", Font.PLAIN, 11);


        frame.add(ts.getMainPanel());
        frame.setJMenuBar(ts.getMenu());

        //frame.setPreferredSize(new Dimension(270, 225));
        frame.pack();
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);

        }

}
