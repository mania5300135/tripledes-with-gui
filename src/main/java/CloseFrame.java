import javax.swing.*;
import java.awt.*;

public class CloseFrame {
    public void closingFrame(JButton button) {
        Container frame = button.getParent();
        do
            frame = frame.getParent();
        while (!(frame instanceof JFrame));
        ((JFrame) frame).dispose();
    }
}
