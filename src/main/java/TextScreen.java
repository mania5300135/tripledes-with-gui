import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class TextScreen extends CloseFrame {
    private JTextArea userText;
    private JRadioButton crypt;
    private JRadioButton uncrypt;
    private JButton make;
    private JPanel MainPanel;
    @Getter
    private JTextArea result;
    private JLabel mode;
    private TextScreen ts = this;
    @Getter
    private JScrollPane sp;
    @Getter
    private JScrollPane sp2;
    @Getter
    private JMenuBar menu;

    /*По умолчанию находимся в текстовом режиме. Смена происходит через меню*/
    private Boolean textMode = true;
    @Setter
    private Boolean canDecrypt = true;

    @Setter
    @Getter
    private int maxLength = -1;
    @Setter
    @Getter
    private int minLength = -1;
    @Setter
    @Getter
    private Boolean haveDigits = false;
    @Setter
    @Getter
    private Boolean haveCapitalLetters = false;
    @Setter
    @Getter
    private Boolean havePunctuationMark = false;

    /*пароли при вводе в окне дешифровки AccessPassPh*/
    @Setter
    private String acceptingPass1;
    @Setter
    private String acceptingPass2;
    @Setter
    private String acceptingPass3;

    /*новые пароли из окна VerPassPh*/
    @Setter
    private String newPass1 = "";
    @Setter
    private String newPass2 = "";
    @Setter
    private String newPass3 = "";

    /*езультат действий с шифрованием/расшифрованием*/
    @Setter
    private String resultText = "";

    private TripleDES tripleDES = new TripleDES();
    private File openedFile;

    public TextScreen() {

        makeMenu();

        crypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                uncrypt.setSelected(false);
            }
        });

        uncrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                crypt.setSelected(false);
            }
        });

        make.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String makeSmth = userText.getText().trim();
                result.setText("");

                if (!makeSmth.isEmpty()) {
                    if (crypt.isSelected()) {
                        if (newPass1.isEmpty() || newPass2.isEmpty() || newPass3.isEmpty()) {
                            JOptionPane.showMessageDialog(null,
                                    "Одна или несколько парольных фраз пусты. " +
                                            "Чтобы изменить парольные фразы, используйте кнопку в меню \"Парольная фраза\"",
                                    "Ошибка в полях значений", JOptionPane.ERROR_MESSAGE);
                        } else {
                            resultText = encrypt("###" + makeSmth);
                            result.setText(resultText);
                            if (mode.getText().equals("файловый")) {
                                int res = JOptionPane.showConfirmDialog(null,
                                        "Удалить файл с шифруемым текстом?",
                                        "Шифровка", JOptionPane.YES_NO_OPTION);
                                if (res == JOptionPane.YES_OPTION) {
                                    openedFile.delete();
                                }
                            }
                        }
                    } else if (uncrypt.isSelected()) {
                        decrypt(makeSmth);
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Выберете одну из операций",
                                "Операция не выбрана", JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(null,
                            "Поле текста пусто. Введите текст. ",
                            "Ошибка в полях значений", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }

    private void makeMenu() {
        JMenuBar menuBar = new JMenuBar();

        Font font = new Font("Verdana", Font.PLAIN, 11);

        JMenu fileMenu = new JMenu("Файл");
        fileMenu.setFont(font);

        JMenuItem openFile = new JMenuItem("Открыть файл");
        openFile.setFont(font);
        openFile.setEnabled(false);
        openFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String res = readFromFile();
                    userText.setText(res);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Возникла проблема с " +
                            "выбранным файлом. Попробуйте еще раз или " +
                            "выберете другой файл", "Ошибка при работе с файлом", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        fileMenu.add(openFile);


        JMenuItem saveRes = new JMenuItem("Cохранить результат");
        saveRes.setFont(font);
        saveRes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String res = result.getText();
                    saveToFile(res);

                } catch (IOException ioException) {
                    ioException.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Возникла проблема с " +
                            "выбранным файлом. Попробуйте еще раз или " +
                            "выберете другой файл", "Ошибка при работе с файлом", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        //saveRes.setEnabled(false);
        fileMenu.add(saveRes);

        JMenuItem saveText = new JMenuItem("Сохранить текст");
        saveText.setFont(font);
        //saveText.setEnabled(false);
        saveText.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String res = userText.getText();
                    saveToFile(res);

                } catch (IOException ioException) {
                    ioException.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Возникла проблема с " +
                            "выбранным файлом. Попробуйте еще раз или " +
                            "выберете другой файл", "Ошибка при работе с файлом", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        fileMenu.add(saveText);

        //вкладка настройки
        JMenu settingsMenu = new JMenu("Настройки");
        settingsMenu.setFont(font);

        JMenu changeMode = new JMenu("Режим работы");
        changeMode.setFont(font);

        JMenuItem textMode = new JMenuItem("Текстовый режим");
        textMode.setFont(font);
        textMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mode.setText("текстовый");
                openFile.setEnabled(false);
                //openCryptoFile.setEnabled(false);
            }
        });
        changeMode.add(textMode);

        JMenuItem fileMode = new JMenuItem("Файловый режим");
        fileMode.setFont(font);
        fileMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mode.setText("файловый");
                openFile.setEnabled(true);
                //openCryptoFile.setEnabled(true);
            }
        });
        changeMode.add(fileMode);

        settingsMenu.add(changeMode);

        JMenuItem passPh = new JMenuItem("Парольная фраза");
        passPh.setFont(font);
        passPh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createCreationPassForm();
            }
        });
        settingsMenu.add(passPh);

        JMenuItem restrictionsOnPass = new JMenuItem("Ограничения на парольную фразу");
        restrictionsOnPass.setFont(font);
        restrictionsOnPass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createRestrictionForm();
            }
        });
        settingsMenu.add(restrictionsOnPass);

        menuBar.add(settingsMenu);

        //пункт меню справки
        JMenu help = new JMenu("Справка");
        help.setFont(font);

        JMenuItem about = new JMenuItem("О программе");
        about.setFont(font);
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(MainPanel, "Выполнила: Кружкова Мария А-05-17.\n" +
                        "Курсовая работа на тему \"TRIPLE DES\"", "О программе", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        help.add(about);

        menuBar.add(help);

        fileMenu.addSeparator();

        JMenuItem exitItem = new JMenuItem("Выход");
        exitItem.setFont(font);
        fileMenu.add(exitItem);

        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        menuBar.add(fileMenu);

        menu = menuBar;
    }

    private String readFromFile() throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int ret = fileChooser.showDialog(null, "Открыть файл");
        if (ret == JFileChooser.APPROVE_OPTION) {
            openedFile = fileChooser.getSelectedFile();
            if (openedFile.canRead()) {
                InputStream inpt = new FileInputStream(openedFile);
                String res = new String(inpt.readAllBytes(), "UTF-8");
                inpt.close();
                return res;
            } else {
                JOptionPane.showMessageDialog(null, "Невозможно " +
                        "прочитать файл", "Ошибка в чтении файла", JOptionPane.ERROR_MESSAGE);
            }
        }

        return "";
    }

    private void saveToFile(String text) throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int ret = fileChooser.showDialog(null, "Сохранить в файл");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file.canWrite()) {
                OutputStream outpt = new FileOutputStream(file);
                outpt.write(text.getBytes("UTF-8"));
                outpt.flush();
                outpt.close();
            } else {
                JOptionPane.showMessageDialog(null, "Невозможно " +
                        "записать в файл", "Ошибка в чтении файла", JOptionPane.ERROR_MESSAGE);
            }
        }


    }

    private void createRestrictionForm() {
        JFrame frame3 = new JFrame();
        frame3.setTitle("Ограничение на парольную фразу");
        frame3.add(new RestrictionOnPassPh(ts).getMainPanel());
        frame3.pack();
        frame3.setLocationRelativeTo(null);
        frame3.setVisible(true);
    }

    private void createCreationPassForm() {
        JFrame frame1 = new JFrame();
        frame1.setTitle("Задание парольной фразы");
        frame1.add(new VerPassPh(ts).getMainPanel());
        frame1.pack();
        frame1.setLocationRelativeTo(null);
        frame1.setVisible(true);
    }

    private void createAccessPassForm(String text) {
        JFrame frame2 = new JFrame();
        frame2.setTitle("Проверка парольной фразы");
        frame2.add(new AccessPassPh(ts, tripleDES, text).getMainPanel());
        frame2.pack();
        frame2.setLocationRelativeTo(null);
        frame2.setVisible(true);
    }

    private String encrypt(String text) {
        String encryptedStr = "";
        if (newPass1.isEmpty() || newPass2.isEmpty() || newPass3.isEmpty()) {
            JOptionPane.showMessageDialog(null,
                    "Одна или несколько парольных фраз пусты. " +
                            "Чтобы изменить парольные фразы, используйте кнопку в меню \"Парольная фраза\"",
                    "Ошибка в полях значений", JOptionPane.ERROR_MESSAGE);
        } else {
            int res = JOptionPane.showConfirmDialog(null, "Будут использованы ранее введеные ключевые слова. " +
                            "Чтобы изменить ключевые слова, используйте кнопку в меню \"Парольная фраза\". Продолжить с текущими ключевыми словами?",
                    "Шифровка", JOptionPane.YES_NO_OPTION);
            if (res == JOptionPane.YES_OPTION) {
                tripleDES.setKey1(newPass1);
                tripleDES.setKey2(newPass2);
                tripleDES.setKey3(newPass3);
                encryptedStr = tripleDES.encrypt(text);
            }
        }
        return encryptedStr;
    }

    private void decrypt(String text) {
        createAccessPassForm(text);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        MainPanel = new JPanel();
        MainPanel.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
        MainPanel.setEnabled(true);
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        MainPanel.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(213, 178), null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add(panel2, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel2.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(11, 75), null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel2.add(spacer2, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Результат:");
        panel2.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        userText = new JTextArea();
        userText.setLineWrap(true);
        panel2.add(userText, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
        result = new JTextArea();
        result.setEnabled(false);
        result.setLineWrap(true);
        panel2.add(result, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Введите свой текст:");
        panel1.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add(panel3, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        panel3.add(panel4, new GridConstraints(0, 0, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(362, 57), null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Выберите криптографическую операцию:");
        panel4.add(label3);
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel4.add(panel5);
        crypt = new JRadioButton();
        crypt.setText("Шифрование");
        panel5.add(crypt, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        uncrypt = new JRadioButton();
        uncrypt.setText("Расшифрование");
        panel5.add(uncrypt, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        panel3.add(spacer3, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        make = new JButton();
        make.setText("Выполнить");
        panel3.add(make, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new FlowLayout(FlowLayout.LEFT, 15, 5));
        panel1.add(panel6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 1, false));
        final JLabel label4 = new JLabel();
        label4.setText("Выбран режим:");
        panel6.add(label4);
        mode = new JLabel();
        mode.setText("текстовый");
        panel6.add(mode);
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        MainPanel.add(panel7, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer4 = new Spacer();
        panel7.add(spacer4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return MainPanel;
    }

}
