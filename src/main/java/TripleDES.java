    import lombok.Setter;

    import java.io.UnsupportedEncodingException;
    import java.math.BigInteger;

    public class TripleDES {

        @Setter
        private String key1;
        @Setter
        private String key2;
        @Setter
        private String key3;
        private DES des;

        public TripleDES(String key1, String key2, String key3) {
            this.key1 = key1;
            this.key2 = key2;
            this.key3 = key3;
            des = new DES();
        }

        public TripleDES() {
            des = new DES();
        }

        public String encrypt (String message) {
            String result;
            result = des.encrypt(key3, des.decrypt(key2, des.encrypt(key1, utfToBin(message))));
            return binToHex(result);

        }

        public String decrypt (String message) {
            String result;
            result = des.decrypt(key1, des.encrypt(key2, des.decrypt(key3, hexToBin(message))));
            return binToUTF(result);


        }

        private static String binToUTF(String bin) {

            // Convert back to String
            byte[] ciphertextBytes = new byte[bin.length()/8];
            String ciphertext = null;
            for(int j = 0; j < ciphertextBytes.length; j++) {
                String temp = bin.substring(0, 8);
                byte b = (byte) Integer.parseInt(temp, 2);
                ciphertextBytes[j] = b;
                bin = bin.substring(8);
            }

            try {
                ciphertext = new String(ciphertextBytes, "utf-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return ciphertext.trim();
        }

        private static String utfToBin(String utf) {

            // Convert to binary
            byte[] bytes = null;
            try {
                bytes = utf.getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String bin = "";
            for (int i = 0; i < bytes.length; i++) {
                int value = bytes[i];
                for (int j = 0; j < 8; j++)
                {
                    bin += ((value & 128) == 0 ? 0 : 1);
                    value <<= 1;
                }
            }
            return bin;
        }

        private static String binToHex(String bin) {

            BigInteger b = new BigInteger(bin, 2);
            String ciphertext = b.toString(16);

            return ciphertext;
        }

        private static String hexToBin(String hex) {

            BigInteger b = new BigInteger(hex, 16);
            String bin = b.toString(2);

            return bin;
        }
    }
